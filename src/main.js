import Vue from 'vue'
import App from './App.vue'

// material design
require('materialize-css'),
require('../scss/materialize.scss'),
require('../scss/grid.scss'),


Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
